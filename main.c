#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<stdio.h>

#include"headers/parser.h"
#include"headers/struct.h"
#include"headers/character.h"

int main ()
{
  // Declare structures
	struct character hero;
  struct world current_world;

	// Take world name for default files
	// add load function
	printf("What is the name of the world?\n");
	scanf("%s", current_world.name);

	// Take character name for save folder
	// Add load function
	printf("What is the name of the protagonist?\n");
	scanf("%s/", hero.name);

	int exists;
  exists = char_check(hero.name, current_world.name);

	// Tests if character exists
	if ( exists == 0 )
	{
		char_init(hero.name, current_world.name);
	}
	else if ( exists == 1 )
	{
		char_load(hero.name, current_world.name);
	}
	else
	{
		printf("char_check failed\n");
		return 0;
	}

	// Calls the parser, runs till quit
	parser(hero.name, current_world.name);

	return 0;
}
