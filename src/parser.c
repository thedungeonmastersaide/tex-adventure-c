#include<stdio.h>
#include<string.h>

#include "../headers/parser.h"
#include "../headers/character.h"

void parser (char hero_name[], char world_name[])
{
	while( 1 == 1 )
	{
		// Make all commands 4 letters
		char input[99];
		printf("❯ ");
		scanf("%s", input);

		if ( strncmp(input, "?", 1) == 0)
		{
			printf("this is the helptext\n");
		}
		else if ( strncmp(input, "quit", 4) == 0)
		{
			printf("exiting\n");
			return;
		}
		else if ( strncmp(input, "take", 4) == 0)
		{
			printf("remove item from room inventory, place in player inventory\n");
		}
		else if ( strncmp(input, "drop", 4) == 0)
		{
			printf("opposite of taken\n");
		}
		else if ( strncmp(input, "look", 4) == 0)
		{
			printf("print text about target\n");
		}
		else if ( strncmp(input, "move", 4) == 0)
		{
			printf("load room target\n");
		}
		else
		{
			printf("error: input not recognized\n");
		}
	}

	return;
}
