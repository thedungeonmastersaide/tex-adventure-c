#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<cjson/cJSON.h>
#include<sys/stat.h>
#include<sys/types.h>

#include"../headers/character.h"
#include"../headers/struct.h"

void drop (char item_name[])
{
  printf("%s\n", item_name);
  return;
}

void take (char item_name[])
{
  printf("%s\n", item_name);
  return;
}

void move (char target_name[])
{
  printf("%s\n", target_name);
  return;
}

void look (char target_name[])
{
  printf("%s\n", target_name);
  return;
}

void char_init (char create_dir[], char world_name[])
{
  mkdir(create_dir, 0700);
  printf("Welcome to %s\n", world_name);

  return;
}

void char_load (char hero_name[], char world_name[])
{
  printf("So, %s, you have returned to %s...\n", hero_name, world_name);
  return;
}

// misused, should return an integer about whether or not characer exists, main then runs char_init/load
int char_check (char hero_name[], char world_name[])
{
  char main_save_dir[] = "/saves/";
  strcat(world_name, main_save_dir);
  strcat(hero_name, "/");
  strcat(world_name, hero_name);

  struct stat sb;

  if (stat(world_name, &sb) == 0 && S_ISDIR(sb.st_mode))
  {
    return 1;
  }
  else
  {
    return 0;
  }
}
