#ifndef STRUCT
#define STRUCT

struct room
{
	char name[64];
	char description[1024];
	const char *items[64];
};

struct item
{
	char name[64];
	char description[1024];
	int weight;
};

struct character
{
	char name[64];
	int health_points;
	int carry_weight;
};

struct world
{
  char name[64];
  char directory[64];
};

#endif
