#ifndef CHARACTER
#define CHARACTER

void take (char item_name[]);
void drop (char item_name[]);
void look (char target_name[]);
void move (char target_name[]);
void char_init (char hero_name[], char world_name[]);
void char_load (char hero_name[], char world_name[]);
int char_check (char hero_name[], char world_name[]);

#endif
